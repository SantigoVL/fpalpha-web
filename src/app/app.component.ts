import { Component } from '@angular/core';
import { HttpClient } from  "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'alpha-web';
  hello:any = '';
  limit: Number = 2000000;
  loading = false;

  constructor(private  httpClient:HttpClient) {}

  ngOnInit() {

  }

  calculate() {
    if(this.limit < 2000000){
      this.limit = 200000;
    }
    this.loading = true;
    //this.httpClient.get<void>("http://apinaruto.naranjacode.com/character/5c577ec9901a382aa2795f7e").
    this.httpClient.get<void>("http://fpalphaservice.naranjacode.com/calculate/" + this.limit).
    subscribe((data:any) => {
      this.hello = data.answer;
      this.loading = false;
    })

/*    fetch("httpx.djfskl")
      .then(res => res.json())
      .then(response => {
        console.log(response);
      })
      .catch(error => console.log(error));
      */
  }

  changed(){
    console.log();
  }


}
